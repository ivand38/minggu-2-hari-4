<?php

require('animal.php');
require('frog.php');
require('ape.php');
$sheep = new Animal("Shaun");

echo "Nama Hewan: ".$sheep->name."<br>";
echo "Jumlah kaki: ".$sheep->legs."<br>"; 
echo "Apakah berdarah dingin? :".$sheep->cold_blooded."<br>"; 

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: ".$sungokong->name."<br>";
echo "Jumlah kaki: ".$sungokong->legs."<br>"; 
echo "Apakah berdarah dingin? :".$sungokong->cold_blooded."<br>"; 
echo $sungokong->yell()."<br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Nama Hewan: ".$kodok->name."<br>";
echo "Jumlah kaki: ".$kodok->legs."<br>"; 
echo "Apakah berdarah dingin? :".$kodok->cold_blooded."<br>"; 
echo $kodok->jump()."<br>" ; // "hop hop"

?>